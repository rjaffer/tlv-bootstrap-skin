var swatchFunctionality = {
  
  init: function() {
    
  }

};

var apparelFunctionality = {

  init: function() {

  }

};

jQuery(document).ready(function() {


    jQuery(".swatch-toggle").toggle(function() {

      jQuery(".swatch").animate({right: 0}, 100);

    }, function() {

      jQuery(".swatch").animate({right: -500}, 100);

    });

  jQuery(".taxonomy-link").hover(function() { jQuery("#" + jQuery(this).data("category")).fadeToggle(); });

  jQuery(".apparel-thumb").each( function() {
                                  var img = jQuery('<img class = "preload" />'); 
                                  img.attr('src', jQuery(this).data("alt"));
                                  img.appendTo('#imagediv');
                                 });

  jQuery("[rel='tooltip']").tooltip();

  jQuery(".apparel-thumb").hover(
                                  function() { 
                                               jQuery(this).animate({opacity: 0}, 500); },
                                  function() { 
                                               jQuery(this).animate({opacity: 1}, 200); } );

  if (jQuery(".messages_product_view_modal").data("message-count") > 0) {
    jQuery(".messages_product_view_modal").modal();
  }

  jQuery("a.gallery_images").fancybox();

  jQuery.items = {};

    jQuery(".swatch-item").each(function() {
      item_id = jQuery(this).attr("id");
      jQuery.items[item_id] = 0;
    });

    item_table_contents = function() {

      var x = 0;

      html = "";

      item_pairs = _.pairs(jQuery.items);

      items = item_pairs.length;

      for (x = 0; x < items; x++) {

        if (item_pairs[x][1] > 0) {
          properties = [jQuery("#" + item_pairs[x][0]).data("color"), jQuery("#" + item_pairs[x][0]).data("name")]
          html = html + "<li class = 'item-li'>" + item_pairs[x][1] + " <div class = 'swatch-color' style='background-color: #" + properties[0] + ";'></div> " + properties[1] + " <a href='#' class='remove-item' data-product-id='" + jQuery("#" + item_pairs[x][0]).data("product-id") + "' data-name='" + item_pairs[x][0] + "'>[x]</a></li>\r\n";
        }


      }

      return html;

    };

      jQuery(document).on("click", ".remove-item", function() {

        item_id = jQuery(this).data("name");
        jQuery.items[item_id]--;

        jQuery("#items").html( item_table_contents);

        jQuery("supergroup[" + jQuery(this).data("product-id") + "]").remove();
        jQuery('<input>').attr('type','hidden').attr('name', "super_group[" + jQuery(this).data("product-id") + "]").attr('value', jQuery.items[item_id]).appendTo('#product_addtocart_form');

      });

      parse_cart_items = function(items) {

      };

      add_cart_item = function(item) {

        items = jQuery.cookie("cart-items").split(",");
        items.push(item);
        jQuery.cookie("cart-items", items.join(","));

      };

    jQuery("#product_addtocart_form").submit(function(e) {

      e.preventDefault();
      return false;

    });

    jQuery(".swatch-item").click(function() {

      jQuery("#item-container").slideDown();

      item_id = jQuery(this).attr("id");
      jQuery.items[item_id]++;


      if (!jQuery.cookie("cart-items")) {
      }

      cookies = jQuery.cookie("cart-items");
      
      add_cart_item(item_id);

      jQuery('<input>').attr('type','hidden').attr('name', "super_group[" + jQuery(this).data("product-id") + "]").attr('value', jQuery.items[item_id]).appendTo('#product_addtocart_form');

      jQuery("#items").html( item_table_contents );


    });

    jQuery(".new-item").popover({trigger: 'hover', placement: 'top', html: true });

});

